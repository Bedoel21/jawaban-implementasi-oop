# 1. Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat. (Lampirkan link source code terkait)
- use case : user bisa melakukan transaksi pembelian pulsa dalam aplikasi

- saya menggunakan variablel :

        static int pulsa;

- seperti pada code di bawah :

            public static void pembelianPulsa() {
                System.out.println("Anda memilih menu Pembelian Pulsa");
                // Kode untuk membeli pulsa tambahan
                Scanner scanner = new Scanner(System.in);
                System.out.println("Silakan pilih nominal pulsa yang akan dibeli:");
                System.out.println("1. Rp5.000");
                System.out.println("2. Rp10.000");
                System.out.println("3. Rp25.000");
                System.out.println("4. Rp50.000");
                System.out.println("Masukkan pilihan Anda: ");
                int pilihan = scanner.nextInt();
                clearscreen();
                int persetujuan = 0;
                switch (pilihan) {
                    case 1:
                        while (persetujuan != 1) {
                            System.out.println("anda memilih pulsa dengan nominal Rp. 5000, jka benat ketik 1");
                            System.out.print("Masukkan pilihan Anda: ");
                            persetujuan = scanner.nextInt();
                            switch (persetujuan) {
                                case 1:
                                    // kode persetujuan pembelian
                                    System.out.println("pulsa sebesar Rp.5000 berhasil di tambahkan");
                                    pulsa += 5000;
                                    break;
                                default:
                                    System.out.println("Pilihan tidak valid!");
                            }
                        }
                        break;


# 2. Mampu menjelaskan algoritma dari solusi yang dibuat (Lampirkan link source code terkait)
- use case : user bisa melakukan transaksi pembelian paket kuota data dengan berbagai macam pilihan tanpa perlu dengan lebih mudah

- solusi yang pakai :
    saya menggunakan metodi switch case dalam switch case, jadi nanti user bisa gampang dalam memilih opsi opsi yang ada, dan penggunaan looping untuk memudahkan user, jika nanti terdapat keslalah pada inputan user tidak perlu dari awal untuk mengulang proses transaksi

- seperti pada code di bawah :
        
        
        public static void pembelianPaketData() {
        // Kode untuk memilih paket internet
                System.out.println("Anda memilih menu Pembelian Paket Data");
                System.out.println("Pilihan paket internet:");
                System.out.println("1. Paket Internet Harian");
                System.out.println("2. Paket Internet Mingguan");
                System.out.println("3. Paket Internet Bulanan");
                System.out.print("Masukkan pilihan Anda: ");
                Scanner scanner = new Scanner(System.in);
                int pilihan = scanner.nextInt();
                clearscreen();

                switch (pilihan) {
                    case 1:
                        System.out.println("Pilihan paket internet:");
                        System.out.println("1. Paket Internet Harian 1GB - Rp5.000");
                        System.out.println("2. Paket Internet Harian 3GB - Rp10.000");
                        System.out.println("3. Paket Internet Harian 5GB - Rp15.000");
                        System.out.print("Masukkan pilihan Anda: ");
                        int pilihanPaketData = scanner.nextInt();
                        clearscreen();
                        int persetujuan = 0;
                        switch (pilihanPaketData) {
                            case 1:
                                // kode untuk membeli paket data 1GB - Rp5.000
                                while (persetujuan != 1) {
                                    System.out.println("anda memilih paket harian 1 GB, jka benat ketik 1");
                                    System.out.print("Masukkan pilihan Anda: ");
                                    persetujuan = scanner.nextInt();
                                    switch (persetujuan) {
                                        case 1:
                                            // kode persetujuan pembelian
                                            if (pulsa >= 5000) {
                                                System.out.println("paket harina 1 Gb berhasil di tambahkan");
                                                kuota += 1;
                                                pulsa -= 5000;
                                            } else {
                                                System.out.println("pulsa tidak cukup");
                                            }

                                            break;
                                        default:
                                            System.out.println("Pilihan tidak valid!");
                                            break;
                                    }
                                }
                                break;
                            case 2:
                                // kode untuk membeli paket data 3GB - Rp10.000
                                while (persetujuan != 1) {
                                    System.out.println("anda memilih paket harian 3 GB, jka benat ketik 1");
                                    System.out.print("Masukkan pilihan Anda: ");
                                    persetujuan = scanner.nextInt();
                                    switch (persetujuan) {
                                        case 1:
                                            // kode persetujuan pembelian
                                            if (pulsa >= 10000) {
                                                System.out.println("paket harina 3 Gb berhasil di tambahkan");
                                                kuota += 3;
                                                pulsa -= 10000;
                                            } else {
                                                System.out.println("pulsa tidak cukup");
                                            }

                                            break;
                                        default:
                                            System.out.println("Pilihan tidak valid!");
                                            break;
                                    }
                                }
                                break;
                            case 3:
                                // kode untuk membeli paket data 5GB - Rp15.000
                                while (persetujuan != 1) {
                                    System.out.println("anda memilih paket harian 5 GB, jka benat ketik 1");
                                    System.out.print("Masukkan pilihan Anda: ");
                                    persetujuan = scanner.nextInt();
                                    switch (persetujuan) {
                                        case 1:
                                            // kode persetujuan pembelian
                                            if (pulsa >= 15000) {
                                                System.out.println("paket harina 5 Gb berhasil di tambahkan");
                                                kuota += 5;
                                                pulsa -= 15000;
                                            } else {
                                                System.out.println("pulsa tidak cukup");
                                            }

                                            break;
                                        default:
                                            System.out.println("Pilihan tidak valid!");
                                            break;
                                    }
                                }
                                break;
                        }
                        break;

                    case 2:
                        System.out.println("Anda memilih Paket Internet Mingguan");
                        System.out.println("Pilihan paket internet:");
                        System.out.println("1. Paket Internet Mingguan 5GB - Rp25.000");
                        System.out.println("2. Paket Internet Mingguan 7GB - Rp30.000");
                        System.out.println("3. Paket Internet Mingguan 10GB -Rp45.000");
                        System.out.print("Masukkan pilihan Anda: ");
                        pilihanPaketData = scanner.nextInt();
                        clearscreen();
                        persetujuan = 0;
                        switch (pilihanPaketData) {
                            case 1:
                                // kode untuk membeli paket data Mingguan 5GB - Rp25.000
                                while (persetujuan != 1) {
                                    System.out.println("anda memilih paket Mingguan 5GB, jka benat ketik 1");
                                    System.out.print("Masukkan pilihan Anda: ");
                                    persetujuan = scanner.nextInt();
                                    switch (persetujuan) {
                                        case 1:
                                            // kode persetujuan pembelian
                                            if (pulsa >= 25000) {
                                                System.out.println("paket Mingguan 5GB berhasil di tambahkan");
                                                kuota += 5;
                                                pulsa -= 25000;
                                            } else {
                                                System.out.println("pulsa tidak cukup");
                                            }

                                            break;
                                        default:
                                            System.out.println("Pilihan tidak valid!");
                                            break;
                                    }
                                }
                                break;
                            case 2:
                                // kode untuk membeli paket Mingguan 7GB - Rp30.000
                                while (persetujuan != 1) {
                                    System.out.println("anda memilih paket Mingguan 7GB, jka benat ketik 1");
                                    System.out.print("Masukkan pilihan Anda: ");
                                    persetujuan = scanner.nextInt();
                                    switch (persetujuan) {
                                        case 1:
                                            // kode persetujuan pembelian
                                            if (pulsa >= 30000) {
                                                System.out.println("paket Mingguan 7GB berhasil di tambahkan");
                                                kuota += 7;
                                                pulsa -= 30000;
                                            } else {
                                                System.out.println("pulsa tidak cukup");
                                            }

                                            break;
                                        default:
                                            System.out.println("Pilihan tidak valid!");
                                            break;
                                    }
                                }
                                break;
                            case 3:
                                // kode untuk membeli paket data Mingguan 10GB -Rp45.000
                                while (persetujuan != 1) {
                                    System.out.println("anda memilih paket Mingguan 10GB, jka benat ketik 1");
                                    System.out.print("Masukkan pilihan Anda: ");
                                    persetujuan = scanner.nextInt();
                                    switch (persetujuan) {
                                        case 1:
                                            // kode persetujuan pembelian
                                            if (pulsa >= 45000) {
                                                System.out.println("paket Mingguan 10GB berhasil di tambahkan");
                                                kuota += 10;
                                                pulsa -= 45000;
                                            } else {
                                                System.out.println("pulsa tidak cukup");
                                            }

                                            break;
                                        default:
                                            System.out.println("Pilihan tidak valid!");
                                            break;
                                    }
                                }
                                break;
                        }
                        break;

                    case 3:
                        System.out.println("Anda memilih Paket Internet Bulanan");
                        System.out.println("Pilihan paket internet:");
                        System.out.println("1. Paket Internet Bulanan 15GB - Rp50.000");
                        System.out.println("2. Paket Internet Bulanan 21GB - Rp70.000");
                        System.out.println("3. Paket Internet Bulanan 32GB - Rp85.000");
                        System.out.print("Masukkan pilihan Anda: ");
                        pilihanPaketData = scanner.nextInt();
                        clearscreen();
                        persetujuan = 0;
                        switch (pilihanPaketData) {
                            case 1:
                                // kode untuk membeli paket data Bulanan 15GB - Rp50.000
                                while (persetujuan != 1) {
                                    System.out.println("anda memilih paket Bulanan 15GB, jka benat ketik 1");
                                    System.out.print("Masukkan pilihan Anda: ");
                                    persetujuan = scanner.nextInt();
                                    switch (persetujuan) {
                                        case 1:
                                            // kode persetujuan pembelian
                                            if (pulsa >= 50000) {
                                                System.out.println("paket Bulanan 15GB berhasil di tambahkan");
                                                kuota += 15;
                                                pulsa -= 50000;
                                            } else {
                                                System.out.println("pulsa tidak cukup");
                                            }

                                            break;
                                        default:
                                            System.out.println("Pilihan tidak valid!");
                                            break;
                                    }
                                }
                                break;
                            case 2:
                                // kode untuk membeli paket Bulanan 21GB - Rp70.000
                                while (persetujuan != 1) {
                                    System.out.println("anda memilih paket Bulanan 21GB, jka benat ketik 1");
                                    System.out.print("Masukkan pilihan Anda: ");
                                    persetujuan = scanner.nextInt();
                                    switch (persetujuan) {
                                        case 1:
                                            // kode persetujuan pembelian
                                            if (pulsa >= 70000) {
                                                System.out.println("paket Bulanan 21GB berhasil di tambahkan");
                                                kuota += 21;
                                                pulsa -= 70000;
                                                break;
                                            } else {
                                                System.out.println("pulsa tidak cukup");
                                            }

                                            break;
                                        default:
                                            System.out.println("Pilihan tidak valid!");
                                            break;
                                    }
                                }
                                break;
                            case 3:
                                // kode untuk membeli paket data Bulanan 32GB - Rp85.000
                                while (persetujuan != 1) {
                                    System.out.println("anda memilih paket Bulanan 32GB, jka benat ketik 1");
                                    System.out.print("Masukkan pilihan Anda: ");
                                    persetujuan = scanner.nextInt();
                                    switch (persetujuan) {
                                        case 1:
                                            // kode persetujuan pembelian
                                            if (pulsa >= 85000) {
                                                System.out.println("paket Bulanan 32GB berhasil di tambahkan");
                                                kuota += 32;
                                                pulsa -= 85000;
                                            } else {
                                                System.out.println("pulsa tidak cukup");
                                            }

                                            break;
                                        default:
                                            System.out.println("Pilihan tidak valid!");
                                            break;
                                    }
                                }
                                break;
                        }
                        break;
                    default:
                        System.out.println("Pilihan tidak valid, silakan coba lagi");
                        pembelianPaketData();
                        break;
                }
            }

        }

# 3. Mampu menjelaskan konsep dasar OOP
- **OOP (Object-Oriented Programming)** adalah paradigma pemrograman yang menggunakan konsep objek sebagai unit dasar untuk memodelkan dan membangun program. Dalam OOP, program terdiri dari objek yang saling berinteraksi, dan konsep dasar yang terkait dengan OOP meliputi:

    1. **Class (Kelas)**: Kelas adalah cetak biru atau blueprint yang mendefinisikan atribut dan metode yang akan dimiliki oleh objek. Kelas berfungsi sebagai template untuk menciptakan objek.

    2. **Object (Objek)**: Objek adalah instance konkret yang dibuat berdasarkan kelas. Setiap objek memiliki atribut (data) yang mewakili keadaan objek tersebut dan metode (fungsi) yang menggambarkan perilaku objek.

    3. **Method (Metode)**: Metode adalah fungsi yang terkait dengan objek tertentu. Metode digunakan untuk melakukan tindakan atau manipulasi data pada objek. Metode juga dapat berinteraksi dengan objek lain.

    4. **Attribute (Atribut)**: Atribut adalah variabel yang terkait dengan objek tertentu. Atribut mewakili keadaan atau data yang dimiliki oleh objek. Atribut juga dapat disebut sebagai properti objek.

    5. **Access Modifier (Modifikasi Akses)**: Modifikasi akses mengontrol aksesibilitas atribut dan metode pada objek atau kelas. Terdapat beberapa modifikasi akses seperti public (dapat diakses dari mana saja), private (hanya dapat diakses di dalam kelas tersebut), dan protected (hanya dapat diakses di dalam kelas tersebut dan kelas turunannya).

    6. **Constructor**: Constructor adalah metode khusus yang digunakan untuk membuat objek dari kelas. Constructor biasanya digunakan untuk menginisialisasi nilai awal atribut pada saat objek dibuat.

- Selain konsep dasar di atas, terdapat juga 4 pilar OOP yang merupakan prinsip utama dalam OOP:

    1. **Inheritance (Pewarisan)**: Inheritance memungkinkan kelas baru (kelas turunan atau subclass) untuk mewarisi atribut dan metode dari kelas yang sudah ada (kelas induk atau superclass). Hal ini memungkinkan penggunaan kembali kode dan membangun hierarki kelas.

    2. **Encapsulation (Enkapsulasi)**: Enkapsulasi menggabungkan data (atribut) dan metode (fungsi) terkait ke dalam satu unit yang disebut objek. Enkapsulasi membatasi akses langsung ke data objek dan mendorong penggunaan metode untuk berinteraksi dengan objek tersebut.

    3. **Abstraction (Abstraksi)**: Abstraksi memungkinkan penyembunyian detail internal suatu objek dan hanya mengekspos fitur penting atau antarmuka yang relevan. Abstraksi membantu dalam memfokuskan pada penggunaan objek tanpa perlu memahami implementasi internalnya.

    4. **Polymorphism (Polimorfisme)**: Polimorfisme memungkinkan objek dengan jenis yang berbeda untuk merespons metode yang sama dengan cara yang berbeda. Dengan polimorfisme, kita dapat menggunakan objek dari kelas yang berbeda dengan cara yang seragam, meningkatkan fleksibilitas dan modularitas program.

    Keempat pilar ini membantu dalam mengorganisasi dan membangun program yang lebih fleksibel, modular, dan mudah diubah atau dikembangkan kembali.


# 4. Mampu mendemonstrasikan penggunaan Encapsulation secara tepat  (Lampirkan link source code terkait)

- **Encapsulation** adalah konsep dalam Pemrograman Berorientasi Objek yang memungkinkan penyembunyian detail implementasi dari objek, sehingga hanya method-method tertentu yang dapat mengakses dan memanipulasi data dalam objek tersebut. Dengan menggunakan encapsulation, kita dapat memperbaiki keamanan dan modularitas aplikasi serta membuatnya lebih mudah untuk dimodifikasi dan diperbaiki di masa depan.

- **contoh penggunaan Encapsulation dalam sebuah codingan** : 

    Berikut contoh program Java yang menerapkan encapsulation dengan membuat kelas Person yang memiliki properti name dan age, serta method `getName()`, `setName()`, `getAge()`, dan `setAge()`. Kedua properti tersebut dideklarasikan sebagai private sehingga tidak dapat diakses langsung dari luar kelas. Sebagai gantinya, kita menggunakan method `getName()`,` setName()`, `getAge()`, dan `setAge()` untuk mengakses dan memanipulasi data dalam objek Person.
       
    **berikut codingannya** :

      public class Person {
          private String name;
            private int age;

            public Person(String name, int age) {
                this.name = name;
                this.age = age;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getAge() {
                return age;
            }

            public void setAge(int age) {
                this.age = age;
            }
        }

        public class Main {
            public static void main(String[] args) {
                Person person = new Person("John Doe", 30);
                System.out.println("Name: " + person.getName());
                System.out.println("Age: " + person.getAge());
                person.setName("Jane Doe");
                person.setAge(25);
                System.out.println("Name: " + person.getName());
                System.out.println("Age: " + person.getAge());
            }
        }

    outputnya sebagai berikut :

> ![dok_oop](dok_oop/enkapsulasi.png)


# 5. Mampu mendemonstrasikan penggunaan Abstraction secara tepat (Lampirkan link source code terkait)
- keuntungan penggunaan abstraksi antara lain:
    1. Mempermudah pengembangan aplikasi Dengan mengidentifikasi fitur penting dari suatu objek, kita dapat menyederhanakan desain dan implementasi aplikasi yang kompleks.
    2. Mempermudah pemeliharaan aplikasi Dengan menggunakan abstraksi, kita dapat membuat perubahan pada kelas abstrak atau interface tanpa mengganggu kelas-kelas turunannya.
    3. Memungkinkan penggunaan polimorfisme Dengan membuat kelas abstrak atau interface, kita dapat menggunakan polimorfisme untuk mengakses objek-objek yang berbeda namun memiliki fitur yang sama.

- **contoh penggunaan abstraksi dalam sebuah codingan**:

    Di sini kita membuat kelas abstrak `Hewan` yang memiliki method abstrak `makan()` dan `bergerak()`. Kemudian kita membuat kelas turunan Kucing dan Anjing yang mengimplementasikan method tersebut sesuai dengan karakteristik masing-masing hewan.

    **berikut codingannya** :

        abstract class Hewan {
        abstract void makan();
        abstract void bergerak();
        }
        class Kucing extends Hewan {
        private String nama;

        public Kucing(String nama) {
            this.nama = nama;
        }

        @Override
        void makan() {
            System.out.println(nama + " sedang makan ikan.");
        }

        @Override
        void bergerak() {
            System.out.println(nama + " sedang berjalan dengan empuk.");
            }
        }

        class Anjing extends Hewan {
        private String nama;

        public Anjing(String nama) {
            this.nama = nama;
        }

        @Override
        void makan() {
            System.out.println(nama + " sedang makan daging.");
        }

        @Override
        void bergerak() {
            System.out.println(nama + " sedang berlari dengan cepat.");
            }
        }

        public class Main {
        public static void main(String[] args) {
            Hewan kucing = new Kucing("Mimi");
            kucing.makan();
            kucing.bergerak();

            Hewan anjing = new Anjing("Max");
            anjing.makan();
            anjing.bergerak();
            }
        }

    outputnya sebagai berikut :

> ![dok_oop](dok_oop/abstraksi.png)

# 6. Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat  (Lampirkan link source code terkait)

- Penggunaan **Inheritance** dalam pemrograman berorientasi objek memungkinkan kita untuk mengambil sifat dan perilaku dari kelas lain yang sudah ada dan memperluas atau memodifikasinya sesuai kebutuhan. Dengan demikian, kita dapat membuat kelas turunan yang lebih spesifik dengan kode yang lebih efisien dan mudah dikelola.

- Sedang, **Polymorphism** memungkinkan kita untuk menggunakan objek yang sama dengan cara yang berbeda, tergantung pada konteksnya. Dengan demikian, kita dapat membuat kode yang lebih fleksibel, mudah dipelihara, dan dapat diubah dengan cepat.

- **contoh penggunaan Inheritance &  Polymorphism dalam sebuah codingan**:

    Dalam contoh ini, kita memiliki kelas abstrak `Shape` sebagai kelas dasar yang memiliki metode abstrak `calculateArea()` untuk menghitung luas. Kemudian, kita memiliki kelas `Rectangle` dan `Circle` yang mewarisi kelas `Shape`.

    Kelas `Rectangle` dan `Circle` mengimplementasikan metode `calculateArea()` untuk menghitung luas persegi panjang dan lingkaran, sesuai dengan karakteristik masing-masing bentuk.

    Di dalam metode `main()`, kita membuat objek rectangle yang merupakan objek `Rectangle` dan objek circle yang merupakan objek `Circle`. Meskipun keduanya dideklarasikan sebagai tipe `Shape`, kita dapat memanggil metode `calculateArea()` pada masing-masing objek.

    **berikut codingannya** :

            abstract class Shape {
            public abstract double calculateArea();
        }

        class Rectangle extends Shape {
            private double length;
            private double width;

            public Rectangle(double length, double width) {
                this.length = length;
                this.width = width;
            }

            @Override
            public double calculateArea() {
                return length * width;
            }
        }

        class Circle extends Shape {
            private double radius;

            public Circle(double radius) {
                this.radius = radius;
            }

            @Override
            public double calculateArea() {
                return Math.PI * radius * radius;
            }
        }

        public class Main {
            public static void main(String[] args) {
                Shape rectangle = new Rectangle(4, 5);
                Shape circle = new Circle(3);

                double rectangleArea = rectangle.calculateArea();
                double circleArea = circle.calculateArea();

                System.out.println("Rectangle Area: " + rectangleArea);
                System.out.println("Circle Area: " + circleArea);
            }
        }


    Dalam contoh ini, **inheritance** terjadi ketika kelas `Rectangle` dan `Circle` mewarisi kelas `Shape`. **Polimorfisme** terjadi ketika kita menggunakan tipe `Shape` untuk mendeklarasikan objek `rectangle` dan `circle`, namun perilaku metode `calculateArea()` yang dipanggil bergantung pada jenis objek yang sebenarnya saat ini.

    outputnya sebagai berikut :

> ![dok_oop](dok_oop/inheri___polymorph.png)


# 7. Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP ?

### Mendigitalisasi proses bisnis dari produk digital yakni "myIM3" ke dalam bentuk Pemrograman berorientasi objek

- Stategi yang saya kerjakan hampir mirip seperti apa yang disampaikan dalam kelas, yakni terdapat 3 tahapan diantaranya :
    1. **Tabel Use case**
    dimana pada tahapan ini kita menentukan "aksi" apa yang bisa di lakukan oleh "User" dan memberikan "nilai prioritas" terhadap si "aksi" untuk menentukan mana "aksi" terpenting yang ada dalam produk digital tersebut
    2. **Class Diagram**
    ini merupakan tahapan ke - 2 setelah use case, dimana pada tahapan ini kita menggambarkan struktur sistem denfan menunjukkan sistem class, atribut, metode, dan hubungan antar objek yang ada.
    3. **Coding**
    ini merupakan tahapan terakhir, dimana kita mengimplentasikan semua yang kita rancang di ke - 2 tahap sebelumnya.

- Pada kesempatan ini saya hanya mengerjakan ke-3 tahapan diatas, dimulai dari use case table, class diagram, serta dilanjut dengan codingan programnya, serta berikut merupakan codingan yang sejauh ini saya sudah buat :

        import java.util.Scanner;

        abstract class registrasi {
            public abstract void login();

            public abstract void profil();

        }

        class User extends registrasi {
            private String nomor;
            private String username;
            private String password;
            private String email;

            public User(String nomor, String username, String password, String email) {
                this.nomor = nomor;
                this.username = username;
                this.password = password;
                this.email = email;
            }

            @Override
            public void login() {
                System.out.println("=======================");
                System.out.println("      Login My IM3");
                System.out.println("=======================");
                System.out.print("Masukkan nomor hp : ");
                Scanner no = new Scanner(System.in);
                nomor = no.nextLine();
                System.out.print("Silahkan buat password : ");
                password = no.nextLine();
                Apps.clearscreen();
            }

            @Override
            public void profil() {
                System.out.println("======================");
                System.out.println("    Profil pengguna");
                System.out.println("======================");
                System.out.println("Username         : " + username);
                System.out.println("Nomor pengguna   : " + nomor);
                System.out.println("Email            : " + email);
                System.out.println();
                System.out.println();
                System.out.println("Opsi : ");
                System.out.println("1. Edit ");
                System.out.println("2. Kembali ");
                System.out.print("Pilih Opsi : ");
                Scanner masuk = new Scanner(System.in);
                int pilihan = masuk.nextInt();
                Apps.clearscreen();
                while (pilihan != 2) {
                    switch (pilihan) {
                        case 1:
                            Scanner edit = new Scanner(System.in);
                            System.out.print("Masukkan username baru :");
                            username = edit.nextLine();
                            System.out.print("Masukkan email :");
                            email = edit.nextLine();
                            System.out.println();
                            System.out.println();
                            System.out.println("Profil berhasil diperbarui ");
                            System.out.println();
                            System.out.println();
                            break;
                        case 2:
                            break;
                        default:
                            System.out.println("Pilihan tidak valid, silakan coba lagi");
                            break;
                    }
                    System.out.println("======================");
                    System.out.println("    Profil pengguna");
                    System.out.println("======================");
                    System.out.println("Username         : " + username);
                    System.out.println("Nomor pengguna   : " + nomor);
                    System.out.println("Email            : " + email);
                    System.out.println();
                    System.out.println();
                    System.out.println("Pilih opsi : ");
                    System.out.println("1. Edit ");
                    System.out.println("2. Kembali ");
                    pilihan = masuk.nextInt();
                    Apps.clearscreen();
                }
            }

        }

        class Apps {

            static int pulsa;
            static int kuota;

            public static void clearscreen() {
                System.out.print("\033[H\033[2J");
                System.out.flush();
            }

            public static void cekPulsa() {
                // Kode untuk memeriksa sisa pulsa
                System.out.println("Anda memilih menu Cek Pulsa ");
                System.out.println("Sisa pulsa Anda: " + pulsa);
            }

            public static void cekKuotaData() {
                // Kode untuk memeriksa sisa Kuota Data
                System.out.println("Anda memilih menu Cek Kuota Data");
                System.out.println("Sisa pulsa Anda: " + kuota + "Gb");
            }

            public static void pembelianPulsa() {
                System.out.println("Anda memilih menu Pembelian Pulsa");
                // Kode untuk membeli pulsa tambahan
                Scanner scanner = new Scanner(System.in);
                System.out.println("Silakan pilih nominal pulsa yang akan dibeli:");
                System.out.println("1. Rp5.000");
                System.out.println("2. Rp10.000");
                System.out.println("3. Rp25.000");
                System.out.println("4. Rp50.000");
                System.out.println("Masukkan pilihan Anda: ");
                int pilihan = scanner.nextInt();
                clearscreen();
                int persetujuan = 0;
                switch (pilihan) {
                    case 1:
                        while (persetujuan != 1) {
                            System.out.println("anda memilih pulsa dengan nominal Rp. 5000, jka benat ketik 1");
                            System.out.print("Masukkan pilihan Anda: ");
                            persetujuan = scanner.nextInt();
                            switch (persetujuan) {
                                case 1:
                                    // kode persetujuan pembelian
                                    System.out.println("pulsa sebesar Rp.5000 berhasil di tambahkan");
                                    pulsa += 5000;
                                    break;
                                default:
                                    System.out.println("Pilihan tidak valid!");
                            }
                        }
                        break;

                    case 2:
                        while (persetujuan != 1) {
                            System.out.println("anda memilih pulsa dengan nominal Rp.10.000, jka benat ketik 1");
                            System.out.print("Masukkan pilihan Anda: ");
                            persetujuan = scanner.nextInt();
                            switch (persetujuan) {
                                case 1:
                                    // kode persetujuan pembelian
                                    System.out.println("pulsa sebesar Rp.10.000 berhasil di tambahkan");
                                    pulsa += 10000;
                                    break;
                                default:
                                    System.out.println("Pilihan tidak valid!");
                            }
                        }
                        break;

                    case 3:
                        while (persetujuan != 1) {
                            System.out.println("anda memilih pulsa dengan nominal Rp.25.000, jka benat ketik 1");
                            System.out.print("Masukkan pilihan Anda: ");
                            persetujuan = scanner.nextInt();
                            switch (persetujuan) {
                                case 1:
                                    // kode persetujuan pembelian
                                    System.out.println("pulsa sebesar Rp.25.0000 berhasil di tambahkan");
                                    pulsa += 25000;
                                    break;
                                default:
                                    System.out.println("Pilihan tidak valid!");
                            }
                        }
                        break;

                    case 4:
                        while (persetujuan != 1) {
                            System.out.println("anda memilih pulsa dengan nominal Rp.50.000, jka benat ketik 1");
                            System.out.print("Masukkan pilihan Anda: ");
                            persetujuan = scanner.nextInt();
                            switch (persetujuan) {
                                case 1:
                                    // kode persetujuan pembelian
                                    System.out.println("pulsa sebesar Rp.50.0000 berhasil di tambahkan");
                                    pulsa += 50000;
                                    break;
                                default:
                                    System.out.println("Pilihan tidak valid!");
                            }
                        }
                        break;
                    default:
                        System.out.println("Pilihan tidak valid!");
                        pembelianPulsa();
                }
            }

            public static void pembelianPaketData() {
                // Kode untuk memilih paket internet
                System.out.println("Anda memilih menu Pembelian Paket Data");
                System.out.println("Pilihan paket internet:");
                System.out.println("1. Paket Internet Harian");
                System.out.println("2. Paket Internet Mingguan");
                System.out.println("3. Paket Internet Bulanan");
                System.out.print("Masukkan pilihan Anda: ");
                Scanner scanner = new Scanner(System.in);
                int pilihan = scanner.nextInt();
                clearscreen();

                switch (pilihan) {
                    case 1:
                        System.out.println("Pilihan paket internet:");
                        System.out.println("1. Paket Internet Harian 1GB - Rp5.000");
                        System.out.println("2. Paket Internet Harian 3GB - Rp10.000");
                        System.out.println("3. Paket Internet Harian 5GB - Rp15.000");
                        System.out.print("Masukkan pilihan Anda: ");
                        int pilihanPaketData = scanner.nextInt();
                        clearscreen();
                        int persetujuan = 0;
                        switch (pilihanPaketData) {
                            case 1:
                                // kode untuk membeli paket data 1GB - Rp5.000
                                while (persetujuan != 1) {
                                    System.out.println("anda memilih paket harian 1 GB, jka benat ketik 1");
                                    System.out.print("Masukkan pilihan Anda: ");
                                    persetujuan = scanner.nextInt();
                                    switch (persetujuan) {
                                        case 1:
                                            // kode persetujuan pembelian
                                            if (pulsa >= 5000) {
                                                System.out.println("paket harina 1 Gb berhasil di tambahkan");
                                                kuota += 1;
                                                pulsa -= 5000;
                                            } else {
                                                System.out.println("pulsa tidak cukup");
                                            }

                                            break;
                                        default:
                                            System.out.println("Pilihan tidak valid!");
                                            break;
                                    }
                                }
                                break;
                            case 2:
                                // kode untuk membeli paket data 3GB - Rp10.000
                                while (persetujuan != 1) {
                                    System.out.println("anda memilih paket harian 3 GB, jka benat ketik 1");
                                    System.out.print("Masukkan pilihan Anda: ");
                                    persetujuan = scanner.nextInt();
                                    switch (persetujuan) {
                                        case 1:
                                            // kode persetujuan pembelian
                                            if (pulsa >= 10000) {
                                                System.out.println("paket harina 3 Gb berhasil di tambahkan");
                                                kuota += 3;
                                                pulsa -= 10000;
                                            } else {
                                                System.out.println("pulsa tidak cukup");
                                            }

                                            break;
                                        default:
                                            System.out.println("Pilihan tidak valid!");
                                            break;
                                    }
                                }
                                break;
                            case 3:
                                // kode untuk membeli paket data 5GB - Rp15.000
                                while (persetujuan != 1) {
                                    System.out.println("anda memilih paket harian 5 GB, jka benat ketik 1");
                                    System.out.print("Masukkan pilihan Anda: ");
                                    persetujuan = scanner.nextInt();
                                    switch (persetujuan) {
                                        case 1:
                                            // kode persetujuan pembelian
                                            if (pulsa >= 15000) {
                                                System.out.println("paket harina 5 Gb berhasil di tambahkan");
                                                kuota += 5;
                                                pulsa -= 15000;
                                            } else {
                                                System.out.println("pulsa tidak cukup");
                                            }

                                            break;
                                        default:
                                            System.out.println("Pilihan tidak valid!");
                                            break;
                                    }
                                }
                                break;
                        }
                        break;

                    case 2:
                        System.out.println("Anda memilih Paket Internet Mingguan");
                        System.out.println("Pilihan paket internet:");
                        System.out.println("1. Paket Internet Mingguan 5GB - Rp25.000");
                        System.out.println("2. Paket Internet Mingguan 7GB - Rp30.000");
                        System.out.println("3. Paket Internet Mingguan 10GB -Rp45.000");
                        System.out.print("Masukkan pilihan Anda: ");
                        pilihanPaketData = scanner.nextInt();
                        clearscreen();
                        persetujuan = 0;
                        switch (pilihanPaketData) {
                            case 1:
                                // kode untuk membeli paket data Mingguan 5GB - Rp25.000
                                while (persetujuan != 1) {
                                    System.out.println("anda memilih paket Mingguan 5GB, jka benat ketik 1");
                                    System.out.print("Masukkan pilihan Anda: ");
                                    persetujuan = scanner.nextInt();
                                    switch (persetujuan) {
                                        case 1:
                                            // kode persetujuan pembelian
                                            if (pulsa >= 25000) {
                                                System.out.println("paket Mingguan 5GB berhasil di tambahkan");
                                                kuota += 5;
                                                pulsa -= 25000;
                                            } else {
                                                System.out.println("pulsa tidak cukup");
                                            }

                                            break;
                                        default:
                                            System.out.println("Pilihan tidak valid!");
                                            break;
                                    }
                                }
                                break;
                            case 2:
                                // kode untuk membeli paket Mingguan 7GB - Rp30.000
                                while (persetujuan != 1) {
                                    System.out.println("anda memilih paket Mingguan 7GB, jka benat ketik 1");
                                    System.out.print("Masukkan pilihan Anda: ");
                                    persetujuan = scanner.nextInt();
                                    switch (persetujuan) {
                                        case 1:
                                            // kode persetujuan pembelian
                                            if (pulsa >= 30000) {
                                                System.out.println("paket Mingguan 7GB berhasil di tambahkan");
                                                kuota += 7;
                                                pulsa -= 30000;
                                            } else {
                                                System.out.println("pulsa tidak cukup");
                                            }

                                            break;
                                        default:
                                            System.out.println("Pilihan tidak valid!");
                                            break;
                                    }
                                }
                                break;
                            case 3:
                                // kode untuk membeli paket data Mingguan 10GB -Rp45.000
                                while (persetujuan != 1) {
                                    System.out.println("anda memilih paket Mingguan 10GB, jka benat ketik 1");
                                    System.out.print("Masukkan pilihan Anda: ");
                                    persetujuan = scanner.nextInt();
                                    switch (persetujuan) {
                                        case 1:
                                            // kode persetujuan pembelian
                                            if (pulsa >= 45000) {
                                                System.out.println("paket Mingguan 10GB berhasil di tambahkan");
                                                kuota += 10;
                                                pulsa -= 45000;
                                            } else {
                                                System.out.println("pulsa tidak cukup");
                                            }

                                            break;
                                        default:
                                            System.out.println("Pilihan tidak valid!");
                                            break;
                                    }
                                }
                                break;
                        }
                        break;

                    case 3:
                        System.out.println("Anda memilih Paket Internet Bulanan");
                        System.out.println("Pilihan paket internet:");
                        System.out.println("1. Paket Internet Bulanan 15GB - Rp50.000");
                        System.out.println("2. Paket Internet Bulanan 21GB - Rp70.000");
                        System.out.println("3. Paket Internet Bulanan 32GB - Rp85.000");
                        System.out.print("Masukkan pilihan Anda: ");
                        pilihanPaketData = scanner.nextInt();
                        clearscreen();
                        persetujuan = 0;
                        switch (pilihanPaketData) {
                            case 1:
                                // kode untuk membeli paket data Bulanan 15GB - Rp50.000
                                while (persetujuan != 1) {
                                    System.out.println("anda memilih paket Bulanan 15GB, jka benat ketik 1");
                                    System.out.print("Masukkan pilihan Anda: ");
                                    persetujuan = scanner.nextInt();
                                    switch (persetujuan) {
                                        case 1:
                                            // kode persetujuan pembelian
                                            if (pulsa >= 50000) {
                                                System.out.println("paket Bulanan 15GB berhasil di tambahkan");
                                                kuota += 15;
                                                pulsa -= 50000;
                                            } else {
                                                System.out.println("pulsa tidak cukup");
                                            }

                                            break;
                                        default:
                                            System.out.println("Pilihan tidak valid!");
                                            break;
                                    }
                                }
                                break;
                            case 2:
                                // kode untuk membeli paket Bulanan 21GB - Rp70.000
                                while (persetujuan != 1) {
                                    System.out.println("anda memilih paket Bulanan 21GB, jka benat ketik 1");
                                    System.out.print("Masukkan pilihan Anda: ");
                                    persetujuan = scanner.nextInt();
                                    switch (persetujuan) {
                                        case 1:
                                            // kode persetujuan pembelian
                                            if (pulsa >= 70000) {
                                                System.out.println("paket Bulanan 21GB berhasil di tambahkan");
                                                kuota += 21;
                                                pulsa -= 70000;
                                                break;
                                            } else {
                                                System.out.println("pulsa tidak cukup");
                                            }

                                            break;
                                        default:
                                            System.out.println("Pilihan tidak valid!");
                                            break;
                                    }
                                }
                                break;
                            case 3:
                                // kode untuk membeli paket data Bulanan 32GB - Rp85.000
                                while (persetujuan != 1) {
                                    System.out.println("anda memilih paket Bulanan 32GB, jka benat ketik 1");
                                    System.out.print("Masukkan pilihan Anda: ");
                                    persetujuan = scanner.nextInt();
                                    switch (persetujuan) {
                                        case 1:
                                            // kode persetujuan pembelian
                                            if (pulsa >= 85000) {
                                                System.out.println("paket Bulanan 32GB berhasil di tambahkan");
                                                kuota += 32;
                                                pulsa -= 85000;
                                            } else {
                                                System.out.println("pulsa tidak cukup");
                                            }

                                            break;
                                        default:
                                            System.out.println("Pilihan tidak valid!");
                                            break;
                                    }
                                }
                                break;
                        }
                        break;
                    default:
                        System.out.println("Pilihan tidak valid, silakan coba lagi");
                        pembelianPaketData();
                        break;
                }
            }

        }

        public class MyIm3App extends Apps {
            public static void main(String[] args) {

                Scanner scanner = new Scanner(System.in);

                // bikin login

                User user1 = new User(null, null, null, null);
                user1.login();

                int choice = 0;
                while (choice != 6) {
                    System.out.println("==========================");
                    System.out.println(" Selamat datang di MyIm3");
                    System.out.println("==========================");
                    System.out.println("Silakan pilih menu:");
                    System.out.println("1. Cek Pulsa");
                    System.out.println("2. Pembelian Paket Data");
                    System.out.println("3. Pembelian Pulsa");
                    System.out.println("4. CekKuotaData");
                    System.out.println("5. Profil");
                    System.out.println("6. Keluar");

                    System.out.print("Masukkan pilihan Anda: ");
                    choice = scanner.nextInt();
                    clearscreen();
                    switch (choice) {
                        case 1:
                            cekPulsa();
                            break;
                        case 2:
                            pembelianPaketData();
                            break;
                        case 3:
                            pembelianPulsa();
                            break;
                        case 4:
                            cekKuotaData();
                            break;
                        case 5:
                            user1.profil();
                            break;
                        case 6:
                            System.out.println("Terima kasih telah menggunakan MyIm3");
                            break;
                        default:
                            System.out.println("Opsi yang anda pilih tidak ada, silakan coba lagi");
                            break;
                    }

                    System.out.println();
                }
            }

        }


# 8. Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table  (Lampirkan diagram terkait)

- Berikut beberapa use case yang saya buat :

    https://docs.google.com/spreadsheets/d/1FCfnZi7y1sUzCLsnqMQASfW3MjtBMLCJLYKz1ycYVKk/edit?usp=sharing

    ![dok_oop](dok_oop/use_case.gif)
    

- Berikut class diagram yang saya buat :

    ![dok_oop](dok_oop/class_diagram.png)

- Berikut class diagram menggunakan mermaid.js:
```mermaid    
classDiagram

class MyIm3App {
    - String nomor
    - String username
    - String password
    - String email
    + int pulsa
    + int kuota
    + main args()
}

class User {
    - String nomor
    - String username
    - String password
    - String email
    + login()
    + profil()
}

class Apps {
    + int pulsa
    + int kuota
    + clearscreen()
    + cekPulsa()
    + cekKuotaData()
    + pembelianPulsa()
    + pembelianPaketData()
}

class Registrasi {
    + login()
    + profil()
}

MyIm3App <|-- User
MyIm3App <|-- Apps
Registrasi <|-- User
```

# 9. Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video   (Lampirkan link Youtube terkait)

- Berikut link video Youtube mengenai penjelasan tugas ini:

    https://youtu.be/p8RR2HSNeA0


# 10. Inovasi UX (Lampirkan url screenshot aplikasi di Gitlab / Github)

- Berikut adalah tampilan hasil atau Output dari code yang sudah saya buat :

    ![dok_oop](dok_oop/hasil_apps.gif)
    

